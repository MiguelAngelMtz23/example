﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUD
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Funciones fn = new Funciones();

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = fn.LlenarGrid("select * from datos");
            //bool conectado = fn.Conectar();

            //if(conectado)
            //{
            //    MessageBox.Show("Conectado");
            //}
            //else
            //{
            //    MessageBox.Show("Error al conectar");
            //}
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            string agregar = "insert into datos values(" + txtId.Text +",'" + txtNombre.Text + "',"+ txtEdad.Text+")";
            if (fn.Insertar(agregar))
            {
                MessageBox.Show("Datos Ingresados Correctamente");
                dataGridView1.DataSource = fn.LlenarGrid("select * from datos");
            }
            else
            {
                MessageBox.Show("Error Al Ingresar Datos");
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            string eliminar = "delete from datos where id= " + txtId.Text;
            if (fn.Eliminar(eliminar))
            {
                MessageBox.Show("Dato Eliminado Correctamente");
                dataGridView1.DataSource = fn.LlenarGrid("select * from datos");
            }
            else
            {
                MessageBox.Show("Error Al Eliminar Dato");
            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            string actualizar  = "update datos set nombre =" + txtNombre.Text +"edad= " +txtEdad.Text + " where id =" + txtId.Text;
            if (fn.Actualizar(actualizar))
            {
                MessageBox.Show("Datos Modificados Correctamente");
                dataGridView1.DataSource = fn.LlenarGrid("select * from datos");
            }
            else
            {
                MessageBox.Show("Error Al Modificar Datos");
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = fn.MostrarUsuarioPorId("select * from datos where id =" + txtId.Text);
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtId.Text ="";
            txtNombre.Text = "";
            txtEdad.Text = "";

        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
